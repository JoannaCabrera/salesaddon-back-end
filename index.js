const express = require('express')
const mongoose = require('mongoose')
const app = express()
const cors = require('cors')
const request = require('request')
const parseString = require('xml2js').parseString

app.use(cors())

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://ortegsssss:spci@cluster0.uswqp.mongodb.net/Dev_Database?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
})

app.use(express.json())
app.use(express.urlencoded({ extended: true }))


const userRoutes = require('./routes/user')
const salesOrderRoutes = require('./routes/salesOrder')
const customerRoutes = require('./routes/customerRts')
const productsRoutes = require('./routes/productRts')
const companyRoutes = require('./routes/companyRts')
const currencyRoutes = require('./routes/currencyRts')
const uomRoutes = require('./routes/uomRts')
const paymentTermsRoutes = require('./routes/paymentTermsRts')
const shipToLocationCustomerRoutes = require('./routes/shipToLocationCustomerRts')
const tpsRoutes = require('./routes/rout_tps')
const moqRoutes = require('./routes/rout_moq')


// const creditLimitRoutes = require('./routes/rout_creditLimit')
const sendToBydRoutes = require('./routes/rout_sendToByd')
const queryPriceRoutes = require('./routes/route_queryPrice')

// app.use('/creditLimit', creditLimitRoutes)
app.use('/sendToByd', sendToBydRoutes)
app.use('/queryPriceList', queryPriceRoutes)
app.use('/api/users', userRoutes)
app.use('/api/salesOrder', salesOrderRoutes)
app.use('/api/customer', customerRoutes)
app.use('/api/products', productsRoutes)
app.use('/company', companyRoutes)
app.use('/api/currency', currencyRoutes)
app.use('/api/uom', uomRoutes)
app.use('/api/terms', paymentTermsRoutes)
app.use('/api/shipToLocationCustomer', shipToLocationCustomerRoutes)
app.use('/api/tps', tpsRoutes)
app.use('/api/moq', moqRoutes)
app.post('/getLimit/:accountNameId', (req,res) => {
	let accountId = req.params.accountId


	let xml = `<?xml version="1.0" encoding="utf-8"?>

	<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global">

	<soapenv:Header/>

	<soapenv:Body>

		<glob:AccountsOpenAmountsQueryRequest_sync>

				<AccountOpenAmountsSelection>

					<SelectionByAccountID>

				   <!--Optional:-->

				   <InclusionExclusionCode>I</InclusionExclusionCode>

				   <IntervalBoundaryTypeCode>1</IntervalBoundaryTypeCode>

				   <!--Optional:-->

				   <LowerBoundaryIdentifier>${accountId}</LowerBoundaryIdentifier>

				</SelectionByAccountID>

				</AccountOpenAmountsSelection>

				<ProcessingConditions>

					<QueryHitsMaximumNumberValue>100</QueryHitsMaximumNumberValue>

					<QueryHitsUnlimitedIndicator>false</QueryHitsUnlimitedIndicator>

					<LastReturnedObjectID></LastReturnedObjectID>

				</ProcessingConditions>

	</glob:AccountsOpenAmountsQueryRequest_sync>

	</soapenv:Body>

	</soapenv:Envelope>`
		
	request({

		method: 'POST',

		uri: 'https://my355904.sapbydesign.com/sap/bc/srt/scs/sap/queryaccountopenamountsin',

		auth: {

			'user': '_AOS0001',

			'pass': '@dd0nP@$$',

			'sendImmediately': false

		},

		headers: {

			'Content-Type': 'text/xml'

		  },

		body: xml,

	},

	 function (error, response, body) {

	   if (error) {

		  res.send ("Credit Limit Web Service Error")

	   } else {

		console.log('statusCode:', response && response.statusCode);

		console.log(response.body)

		parseString(body, function(err, result){

		console.log(result)

	   let x = Object.values(result['soap-env:Envelope']['soap-env:Body']['0']['n0:AccountOpenAmountsQueryResponse_sync']['0']['AccountOpenAmounts']['0']['CurrentSpendingLimitAmount']['0'])

		console.log(x)

		res.send(x)

		})

	}})

})

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
})