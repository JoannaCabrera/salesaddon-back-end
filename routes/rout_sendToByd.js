const express = require('express');
const router = express.Router();
const senToBydController = require('../controllers/sendToByd');


router.post('/saleOrderToBYD', function (req, res) {
    senToBydController.sendToByd(req.params).then(result => res.send(result));
})
module.exports = router;