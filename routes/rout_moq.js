const express = require('express');
const moqController = require('../controllers/moqCtrl');
const router = express.Router();

/* Add a Minimum Order Quantity to the Database */
router.post('/add', (req, res) => {
	moqController.addMoq(req.body).then(result => res.send(result));
})

/* View All Minimum Order Quantities from the Database */
router.get('/view/all', (req, res) => {
	moqController.viewAllMoqs().then(result => res.send(result));
})

/* View a Specific Minimum Order Quantity from the Database */
router.get('/view/specific/:productCategoryId', (req, res) => {
	moqController.viewMoq(req.params).then(result => res.send(result));
})

/* Validate a Specific Minimum Order Quantity from the Database */
router.get('/validate/:productCategoryId', (req, res) => {
	moqController.validateMoq(req.params).then(result => res.send(result));
})

module.exports = router;