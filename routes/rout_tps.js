const express = require('express');
const tpsController = require('../controllers/tpsCtrl');
const router = express.Router();

/* Add a Currency to the Database */
router.post('/add', (req, res) => {
	tpsController.addTps(req.body).then(result => res.send(result));
})

/* View all Currencies from the Database */
router.get('/view/all', (req, res) => {
	tpsController.viewAllTps().then(result => res.send(result));
})

/* View a specific Currency from the Database */
router.get('/view/specific/:code', (req, res) => {
	tpsController.viewTps(req.params).then(result => res.send(result));
})


module.exports = router;