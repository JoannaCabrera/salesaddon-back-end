const express = require('express');
const currencyController = require('../controllers/currencyCtrl');
const router = express.Router();

/* Add a Currency to the Database */
router.post('/add', (req, res) => {
	currencyController.addCurrency(req.body).then(result => res.send(result));
})

/* View all Currencies from the Database */
router.get('/view/all', (req, res) => {
	currencyController.viewAllCurrencies().then(result => res.send(result));
})

/* View a specific Currency from the Database */
router.get('/view/:currencyTicker', (req, res) => {
	currencyController.viewCurrency(req.params).then(result => res.send(result));
})


module.exports = router;