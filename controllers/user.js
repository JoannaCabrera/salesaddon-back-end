const User = require('../models/user')
const auth = require('../auth')
const bcrypt = require("bcrypt"); 

/* Register User */
module.exports.register = (reqBody) => {
	let newUser = new User({
		employeeId: reqBody.employeeId,
		userName: reqBody.userName,
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		password: bcrypt.hashSync(reqBody.password, 10),
		department: reqBody.department
	})

	return newUser.save().then((user, error) => {
		if(error){
			return `User registration failed!`;
		} else {
			return `User registration success!`;
		}
	})
};
/* Log-in */
module.exports.login = (reqBody) => {
	return User.findOne({userName : reqBody.userName }).then(result => {
		if (result === null || result === undefined) { 
			return `No username of ${reqBody.userName} is found.`
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect) {
				return { accessToken: auth.createAccessToken(result)}
			} else {
				return `Incorrect Password`
			}	
		}
	})
};

/*Authentication and changing Password*/
module.exports.getUserDetails = (userData) => {
	return User.findOne({userName: userData.userName}).then(result => {
		return result
	})
}

/*Update Password*/
module.exports.passwordchange = (params) => {
	//note
	const update = {
	password: bcrypt.hashSync(params.password,10),
	initialLogin: false
	}
	return User.findByIdAndUpdate(params.userId, update).then((doc, err) => {
		return (err) ? false : true
	})
}

	


