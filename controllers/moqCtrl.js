const MOQ = require('../models/mod_moq');

// Add a TPS to the database
module.exports.addMoq = (reqBody) => {
	let newMOQ = new MOQ ({
		productCategoryId: reqBody.productCategoryId,
		minimumOrderQuantity : reqBody.minimumOrderQuantity
	})

	return newMOQ.save().then((user, error) => {
		if(error){
			return `Error: Failed to add the currency in the database!`;
		} else {
			return `Successfully added the currency in the database!`;
		}
	})

}

// View All TPS from the database
module.exports.viewAllMoqs = () => {
	return MOQ.find().then(result => {
		return result;
	})
}

// View a Specific TPS from the database
module.exports.viewMoq = (reqParams) => {
	return MOQ.findOne({productCategoryId: reqParams.productCategoryId}).then(result => {
		if(Object.keys(result).length < 1) {
			return `No Minimum Order Quantity with a Product Category ID of ${reqParams.productCategoryId}`;
		} else {
			return result
		}
	})
}

/* Validate a Specific Minimum Order Quantity from the Database */
module.exports.validateMoq = (reqParams) => {
	return MOQ.findOne({productCategoryId: reqParams.productCategoryId}).then(result => {
		if(result === null) {
			return false
		} else {
			return true
		}
	})
}