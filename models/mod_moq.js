const mongoose = require('mongoose')

const moqSchema = new mongoose.Schema({
    productCategoryId: {
        type: String,
        require: true
    },
    minimumOrderQuantity: {
        type: String,
        require: true
    }
})

module.exports = mongoose.model('MOQ', moqSchema);