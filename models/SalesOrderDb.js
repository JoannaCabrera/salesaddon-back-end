const mongoose = require('mongoose')

const salesOrderSchema = new mongoose.Schema({
    salesOrderNo: {
        type: String,
        require: true
    },
    accountId : {
        type: String,
        require: true
    },
    accountName: {
        type: String,
        require: true
    },
    shipToPartyId: {
        type: String,
        require: true
    },
    shipToPartyDescription: {
        type: String,
        require: true
    },
    paymentTermsId: {
        type: String,
        require: true
    },
    paymentTerms: {
        type: String,
        require: true
    },
    requestedDate: {
        type: String,
        require: true
    },
    externalReference:{
        type: String,
        require: true
    },
    comments: {
        type: String
    },
    docStatus: {
        type: String,
        require: true
    },
    creationDate: {
        type: String,
        require: true
    },
    requestor: {
        type: String,
        require: true
    },
    employeeId: {
        type: Number,
        require: true
    },
    currency: {
        type: String,
        require: true
    },
    sapSoId: {
        type: String,
        require: true
    },
    salesGroup: {
        type: String,
        require: true
    },
    mbAccountId: {
        type: String,
        require: true
    },
    underTolerance: {
        type: String,
        require: true
    },
    overTolerance: {
        type: String,
        require: true
    },
    deliveryDate: {
        type: String,
        require: true
    },
    items: []
})

module.exports = mongoose.model('SalesOrder', salesOrderSchema);
